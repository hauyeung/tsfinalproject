﻿module famouspainters {
    export class showpainters {
        displaypainter(painterdata: painters.FamousPainter[], name: string) {
            var painter: HTMLElement = document.getElementById('painters');
            for (var i = 0; i < painterdata.length; i++) {
                if (painterdata[i].name.trim() == name.trim()) {
                    console.log(name);
                    console.log(painterdata[i]);
                    painter.innerHTML = '';
                    var paintertext: string = '<h2>' + painterdata[i].name + '</h2>';
                    paintertext += '<h3>Style</h3>';
                    paintertext += '<p>' + painterdata[i].style + '</p>';
                    paintertext += '<h3>Examples</h3>';
                    paintertext += '<p>' + painterdata[i].examples + '</p>';
                    paintertext += '<h3>Birthdate</h3>';
                    paintertext += '<p>' + painterdata[i].birthdate + '</p>';
                    paintertext += '<h3>Birth Place</h3>';
                    paintertext += '<p>' + painterdata[i].birthplace + '</p>';
                    paintertext += '<h3>Nationality</h3>';
                    paintertext += '<p>' + painterdata[i].nationality + '</p>';
                    painter.innerHTML = paintertext;
                }
            }
        }

    }

}