﻿declare module painters {

    export interface FamousPainter {
        name: string;
        style: string;
        examples: string[];
        birthplace: string;
        nationality: string;
        birthdate: string;
    }

    export interface RootObject {
        famousPainters: FamousPainter[];
    }

}

